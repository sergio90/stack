#ifndef stack_h
#define stack_h


/*
This header file simply contains the declarations of the functions.
Provides an "interface" for other c files to see what functions
are available and what parameters to use. But doesn't provide
the implementation (definition) of the functions. See stack_ll.c
to see the implementation using linked lists. In other words,
can implement the stack with either an array, or linked list
without stacktest.c noticing any difference.
*/



typedef struct stack *stack;//pointer to a stack


//allocate memory for a new stack struct
//returns 0 on success, -1 on failure
int init(stack *s /*double pointer*/);


//push a new data item on the stack
//returns 0 on success, -1 on failure
int push(stack s, int data);

//pops a node from the stack and returns the data item via a pointer parameter
//function returns 0 on success, returns -1 on failure
int pop(stack s, int *data);

//returns size of stack
int stacksize(stack s);

//free the stack
//return 0 on success, -1 on error
int freestack(stack s);

//prints the stack
void printstack(stack s);

#endif
