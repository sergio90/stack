#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


typedef struct node{
   int data;
   struct node *next;
}node;

struct stack{
   node *head;
   int size;
};


//allocate memory for a new stack struct
//returns 0 on success, -1 on failure
int init(stack *s /*double pointer*/){
   *s = (stack)malloc(sizeof(struct stack));
   if(*s == NULL){
      return -1;
   }
   (*s)->size = 0;
   (*s)->head = NULL;//remember, it's a double pointer
   return 0;
   
}


//push a new data item on the stack
//returns 0 on success, -1 on failure
int push(stack s, int data){
   node *tmp = (node *)malloc(sizeof(node));
   if(tmp == NULL){
      return -1;
   }
   tmp->next = s->head;
   tmp->data = data;
   s->head = tmp;
   s->size++;
   return 0;
}

//pops a node from the stack and returns the data item via a pointer parameter
//function returns 0 on success, returns -1 on failure
int pop(stack s, int *data){
   if(s->head == NULL){//stack is empty
      return -1;
   }

   node *tmp = s->head;
   *data = tmp->data;

   s->head = tmp->next;
   free(tmp);
   s->size--;
   return 0;
}

//returns size of stack 
int stacksize(stack s){
   return s->size;
}

//free the stack
//return 0 on success, -1 on error
int freestack(stack s){
   if(s == NULL){
      return -1;
   }

   node *tmp;
   while(s->head != NULL){
      tmp = s->head->next;
      free(s->head);
      s->head = tmp;
   }
   free(s);
   
}



void printstack(stack s){
   node *tmp = s->head;

   if(tmp != NULL){
      printf("stack contains:");
      printf("%d",tmp->data);
      tmp = tmp->next;
   }else{
      printf("stack is empty");
   }
   while(tmp!=NULL){
      printf("->%d",tmp->data);
      tmp = tmp->next;
   }
   printf("\n");
}
