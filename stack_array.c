#include <stdio.h>
#include <stdlib.h>
#include "stack.h"



struct stack{
   int *array;
   int size;
};


//returns 0 on success, -1 on failure
int init(stack *s /*double pointer*/){
   return 0;
   
}


//push a new data item on the stack
//returns 0 on success, -1 on failure
int push(stack s, int data){
   return 0;
}

//pops a node from the stack and returns the data item via a pointer parameter
//function returns 0 on success, returns -1 on failure
int pop(stack s, int *data){
   return 0;
}

//returns size of stack 
int stacksize(stack s){
   return s->size;
}

//free the stack
//return 0 on success, -1 on error
int freestack(stack s){
   
   return 0;
}



void printstack(stack s){
   printf("stack contains:");
}
