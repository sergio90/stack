#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*this include provides this c file with
the declarations of the insert, extract, etc.
In other words, lets us call the functions
without providing an implementation. But, you have
to provide an implementation(definition) of
the functions in another file somwhere 
before you can create an executable*/
#include "ordered_queue.h"

#define BLKSIZE 1025


int main(int argc, char * argv[]){
   queue q;
   char input[BLKSIZE];
   char cmd[BLKSIZE];

   int tmp;

   if(init(&q) == -1){
      fprintf(stderr,"error initializing queue\n");
      exit(1);
   }

   printf("queue tester\nto insert type \"insert int\"\nto extract type \"extract\"\nto quit type \"q\" or CTRL+D\n");
   printf("Please don't type a string longer than %d bytes, or you will mess up the program\n",BLKSIZE-1);
   printf("\n>");
   while(fgets(input,BLKSIZE,stdin) != NULL){
      
      if( sscanf(input,"%s",cmd) == 1){//grab the first string user types
         if(strcmp(cmd,"insert") == 0){
            if (sscanf(input,"%s %d",cmd, &tmp) == 2){//get the int following push
               insert(q,tmp,tmp);
               printqueue(q);
            }else{
               printf("invalid int given to push\n");
            }
            
         }else if(strcmp(cmd,"extract") == 0){
            if(extract(q,&tmp) == 0){
               printf("extracted:%d\n",tmp);
               printqueue(q);
            }else{
               printf("error extracting from queue\n");
            }
         }else if(strcmp(cmd,"quit") == 0 || strcmp(cmd,"q") == 0){
            break;
         }else{
            printf("invalid cmd\n");
         }
      }


      printf(">");
   }

   printf("goodbye\n");
   freequeue(q);
}


