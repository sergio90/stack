#include <stdio.h>
#include <stdlib.h>
#include "ordered_queue.h"


/*
   implements a max priority queue with linked lists
   not a good idea in practice most of the time, since more
   efficient implementations will use a heap of
   some kind. This is just for demonstrative purposes
*/



typedef struct node{
   int data;
   int priority;
   struct node *next;
}node;

struct queue{
   node *head;
   int size;
};


//allocate memory for a new queue struct
//returns 0 on success, -1 on failure
int init(queue *q /*double pointer*/){
   *q = (queue)malloc(sizeof(struct queue));
   if(*q == NULL){
      return -1;
   }
   (*q)->size = 0;
   (*q)->head = NULL;//remember, it's a double pointer
   return 0;
   
}


//insert a new data item in the queue, based on priority
//returns 0 on success, -1 on failure
int insert(queue q, int data, int priority){
   node *cur,*prev;
   node *to_insert = (node *)malloc(sizeof(node));
   if(to_insert == NULL){
      return -1;
   }

   to_insert->priority = priority;
   to_insert->data = data;

   cur = q->head;
   prev = q->head;

   //loop through linked list
   //and find place to insert to_insert
   while(cur != NULL){
      if(cur->priority < to_insert->priority){
         break;
      }
      prev = cur;
      cur = cur->next;
   }

   //if to_insert goes at the
   //front of the queue, either to_insert
   //has the highest priority, or queue was empty
   if(cur == q->head){
      to_insert->next = q->head;
      q->head = to_insert;
   }else{ //inserting somwhere else in the queue
      to_insert->next = prev->next;
      prev->next = to_insert;
   }

   q->size++;
   return 0;
}

//pops a node from the queue and returns the data item via a pointer parameter
//function returns 0 on success, returns -1 on failure
int extract(queue q, int *data){
   if(q->head == NULL){//queue is empty
      return -1;
   }

   node *tmp = q->head;
   *data = tmp->data;

   q->head = tmp->next;
   free(tmp);
   q->size--;
   return 0;
}

//returns size of queue 
int queuesize(queue s){
   return s->size;
}

//free the queue
//return 0 on success, -1 on error
int freequeue(queue q){
   if(q == NULL){
      return -1;
   }

   node *tmp;
   while(q->head != NULL){
      tmp = q->head->next;
      free(q->head);
      q->head = tmp;
   }
   free(q);
   
}



void printqueue(queue q){
   node *tmp = q->head;

   if(tmp != NULL){
      printf("queue contains:");
      printf("%d",tmp->data);
      tmp = tmp->next;
   }else{
      printf("queue is empty");
   }
   while(tmp!=NULL){
      printf("->%d",tmp->data);
      tmp = tmp->next;
   }
   printf("\n");
}
