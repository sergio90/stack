#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*this include provides this c file with
the declarations of the pop, push, etc.
In other words, lets us call the functions
without providing an implementation. But, you have
to provide an implementation(definition) of
the functions before you can create an executable*/
#include "stack.h"

#define BLKSIZE 1025


int main(int argc, char * argv[]){
   stack s;
   char input[BLKSIZE];
   char cmd[BLKSIZE];

   int tmp;

   if(init(&s) == -1){
      fprintf(stderr,"error initializing stack\n");
      exit(1);
   }

   printf("stack tester\nto push type \"push int\"\nto pop type \"pop\"\nto quit type \"q\" or CTRL+D\n");
   printf("Please don't type a string longer than %d bytes, or you will mess up the program\n",BLKSIZE-1);
   printf("\n>");
   while(fgets(input,BLKSIZE,stdin) != NULL){
      
      if( sscanf(input,"%s",cmd) == 1){//grab the first string user types
         if(strcmp(cmd,"push") == 0){
            if (sscanf(input,"%s %d",cmd, &tmp) == 2){//get the int following push
               push(s,tmp);
               printstack(s);
            }else{
               printf("invalid int given to push\n");
            }
            
         }else if(strcmp(cmd,"pop") == 0){
            if(pop(s,&tmp) == 0){
               printf("popped:%d\n",tmp);
               printstack(s);
            }else{
               printf("error popping stack\n");
            }
         }else if(strcmp(cmd,"quit") == 0 || strcmp(cmd,"q") == 0){
            break;
         }else{
            printf("invalid cmd\n");
         }
      }


      printf(">");
   }

   printf("goodbye\n");
   freestack(s);
}


