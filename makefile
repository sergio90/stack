CC=gcc

all: linkedlist_stack.exe array_stack.exe linkedlist_ordered_queue.exe

linkedlist_stack.exe: stack_ll.o stacktester.o
	$(CC) -o linkedlist_stack.exe stack_ll.o stacktester.o

array_stack.exe: stack_array.o stacktester.o
	$(CC) -o array_stack.exe stack_array.o stacktester.o

linkedlist_ordered_queue.exe: ordered_queue_ll.o ordered_queue_tester.o
	$(CC) -o linkedlist_ordered_queue.exe ordered_queue_ll.o ordered_queue_tester.o

stacktester.o: stacktester.c stack.h
	$(CC) -c stacktester.c

ordered_queue_tester.o: ordered_queue_tester.c ordered_queue.h
	$(CC) -c ordered_queue_tester.c 

stack_ll.o: stack_ll.c stack.h
	$(CC) -c stack_ll.c

stack_array.o: stack_array.c stack.h
	$(CC) -c stack_array.c

ordered_queue_ll.o: ordered_queue_ll.c ordered_queue.h
	$(CC) -c ordered_queue_ll.c

clean:
	rm *.exe *.o
