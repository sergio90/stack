#ifndef ordered_queue_h
#define ordered_queue_h

typedef struct queue *queue;

//initializes queue
int init(queue *q /*double pointer*/);
//inserts something into the queue
int insert(queue q, int data, int priority);
//extracts the top element
int extract(queue q, int *data);
//returns size of queue
int queuesize(queue s);
//frees dynamic memory associated with queue
int freequeue(queue q);
//prints queue
void printqueue(queue q);
#endif
